// console.log('Hello World')

// Exponent operator

const firstNum = 8 ** 2;
console.log(firstNum);

const secondNum = Math.pow(8,2)
console.log(secondNum)

// 5 raised to the power of 5
const thirdNum = 5**5;
console.log(thirdNum)

// --------------------------------------------------------
// Template Literals
/*
	allows to write strings without using the concatenation operator(+)
*/

let name = "Nehemiah";

// pre-template literal string
// using single quote ('')
let message = 'Hello '+name+' welcome to programming!'
console.log("Message without template literals: "+message)

// strings using template literal
// uses back ticks (``)
message = `Hello ${name}. welcome to programming!`
console.log(message)

let anotherMessage = `
${name} attended a math competition.
He won it by solving the problem 8 **2 with the solution of ${firstNum}
`
console.log(anotherMessage)

anotherMessage = "\n "+name+' attended a math competition. \n He won it by solving the problem 8**2 with the solution of '+firstNum+'.'
console.log(anotherMessage)

const interestRate = .1
const principal = 1000;
console.log(`the interest on your savings: ${principal * interestRate}`)

// ------------------------------------------------------------
// Array destructuring
/*
	Allows to unpack elements in arrays into distinct variable. Allows us to name array elements with variables instead of index numbers

	Syntax:
		let/const [variableName, variableName, variableName] = array
*/

const fullName = ["Joe", "Dela", "Cruz"]

// Pre-Array destructuring
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log(`Hello, ${fullName[0]} ${fullName[1]} ${fullName[2]}`)

// Array Destructuring
const [firstName, middleName, lastName] = fullName
console.log(firstName)
console.log(middleName)
console.log(lastName)
console.log(`Hello ${firstName} ${middleName} ${firstName}! it's nice to meet you!`)

// --------------------------------------------------------------
// object destructure
/*
	allows to unpack properties of objects into distinct variable. shorten the syntax for accessing properties from objects

	syntax:
		let/const {propertyName, propertyName, propertyName} = object

*/

const person = {
	givenName: "Jane",
	maidenName: "Dela",
	familyName: "Cruz"
}

// pre-object destructuring
console.log(person.givenName)
console.log(person.maidenName)
console.log(person.familyName)

const {maidenName, givenName, familyName} = person;
console.log(givenName)
console.log(maidenName)
console.log(familyName)

function getFullName({givenName, maidenName, familyName}) {
	console.log(`${givenName} ${maidenName} ${familyName}`)
}

getFullName(person)


// ----------------------------------------------------
// Arrow FUnctions
/*
	Alternative syntax to traditional functions

	const variableName = () => {
		console.log
	}
*/

const hello = () => {
	console.log("Hello world")
}

hello()

function greeting () {
	console.log("Hello world")
}
greeting()

// Pre-Arrow Function
/*
	Syntax:
		function functionName (parameterA, parameterB) {
		console.log()
		}
*/
function printFullName (firstName, middleInitial, lastName){
	console.log(firstName+" "+middleInitial+" "+lastName)
}

printFullName("Amiel", "D", "Oliva")



// Arrow Function
/*
	Syntax:
		let/const VariableName = (parameterA, parameterB) => {
			console.log()
		}
*/

const printFullName1 = (firstName, middleInitial, lastName) => {
	console.log(`${firstName} ${middleInitial} ${lastName}`)
}

printFullName1("Charles Patrick", "A", "Lilagan")

const students = ["Rupert", "Carlos", "Jerome"];
// Arrow Functions with loops
// Pre-Arrow Function

students.forEach(function(student) {
	console.log(`${student} is a student.`)
})

// Arrow Function
students.forEach((student) => {
	console.log(`${student} is a student.`)
})


//------------------------------------------------------- 
// implicit return statement

// pre-arrow function
function add(x,y) {
	return x+y
}
let total = add(12,15)
console.log(total)


// Arrow function
const addition = (x,y) => x+y;
/*const addtition = (x,y) => {
	return x+y
}*/
// pag lalagyan ng return, maglagay ng curly braces


let resultOfAdd = addition(12,15)
console.log(resultOfAdd)


// ------------------------------------------------------------
// Default function argument value

const greet = (name = "User") => {   
	return `Good morning, ${name}`
}
console.log(greet())
// maguundefined pag walang sinet na value for parameter sa greet()
//yung "User" ang default na lalabas pag walang sinet na value sa pag invoke
console.log(greet("Grace"))



// ------------------------------------------------------------
// Class Based Object Blueprints
/*
	Allows creation/instantiation of objects using classes as blueprints

	Syntax:
		class className {
			constructor (objectPropertyA, objectPropertyB) {
				this.objectPropertyA = objectPropertyA;
				this.objectPropertyB = objectPropertyB;
			}
		}

*/


class car {
	constructor (brand, name, year) {
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}

const myCar = new car();
console.log(myCar)

myCar.brand = "Ford"
myCar.name = "Ranger Raptor"
myCar.year = "2021"

console.log(myCar)

const myNewCar = new car ("toyota", "vios", "2021")
console.log(myNewCar)